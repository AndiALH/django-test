from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('library/', views.library, name='library'),
    path('itemlist/', views.itemlist, name='itemlist'),
    path('create_item/', views.item_create_view, name='createitem'),
    path('item/<int:id>/', views.item_desc, name='itemdesc'),
    path('item/<int:id>/delete/', views.item_delete, name='itemdelete'),
    path('test_template/', views.testing_templates, name='testtemplate'),
]