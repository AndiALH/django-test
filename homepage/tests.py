from django.test import TestCase, Client, LiveServerTestCase
# from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import resolve
from . import views
from django.contrib import auth
from django.contrib.auth.models import User
# from .models import Status
# from .forms import Status_Form
# from django.utils import timezone
# from selenium import webdriver
# import unittest
# import time
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options


# Create your tests here.
class HomePageTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super(HomePageTest, cls).setUpClass()
        cls.user = User.objects.create_user(
            'username101', 'thisisemail@gmail.com', 'password123'
        )
        cls.user.first_name = 'firstname'
        cls.user.last_name = 'lastname'
        cls.user.save()

    def test_homepage_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_homepage_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_homepage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    #ref : https://stackoverflow.com/questions/5660952/test-that-user-was-logged-in-successfully
    def test_homepage_not_logged_in(self):
        response = self.client.get('/')
        html = response.content.decode()
        user = auth.get_user(self.client)
        assert user.is_anonymous
        self.assertIn('is authenticated:False', html)
        self.assertNotIn('is authenticated:True', html)
        # self.assertNotIn(user.email, html)

    def test_homepage_logged_in(self):
        self.client.login(username='username101', password='password123')
        response = self.client.get('/')
        html = response.content.decode()
        user = auth.get_user(self.client)
        assert user.is_authenticated
        self.assertIn('is authenticated:True', html)
        self.assertNotIn('is authenticated:False', html)
        self.assertIn(user.email, html)

    # def test_model_can_create_new_status(self):
    #     new_status = Status.objects.create(status='Syaayaa')

    #     count_status = Status.objects.all().count()
    #     self.assertEqual(count_status, 1)
    #     self.assertEqual(str(new_status), new_status.status)

    # def test_can_save_POST_request(self):
    #     response = self.client.post('/add_status/', data={'status' : 'anjay mabar'})
    #     count_status = Status.objects.all().count()
    #     self.assertEqual(count_status, 1)

    #     self.assertEqual(response.status_code, 302)
    #     self.assertEqual(response['location'], '/')

    #     new_response = self.client.get('/')
    #     html_response = new_response.content.decode('utf8')
    #     self.assertIn('anjay mabar', html_response)

    def test_using_template_from_other_app(self):
        response = Client().get('/test_template/')
        self.assertTemplateUsed(response, 'test_template.html')
