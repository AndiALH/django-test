from django import forms

from .models import Item

class ItemForm(forms.ModelForm):
    name = forms.CharField(widget=forms.Textarea(attrs={"placeholder": "your name", "rows": 1}))
    email = forms.EmailField()
    description = forms.CharField(required=False, widget=forms.Textarea(
                                                            attrs={
                                                                "placeholder": "your description",
                                                                "class": "new-class-name two",
                                                                "id": "my-id-for-textarea",
                                                                "rows": 15,
                                                                "cols": 100,
                                                            }
                                                        )
                                                    )
    price = forms.DecimalField(initial=199.99)
    class Meta:
        model = Item
        fields = [
            'name',
            'description',
            'price'
        ]
    
    def clean_name(self, *args, **kwargs):
        name = self.cleaned_data.get("name")
        # mengecek apakah ada string "item" di nama Item
        if not "item" in name:
            raise forms.ValidationError("This is not a valid name")
        return name
    
    def clean_email(self, *args, **kwargs):
        email = self.cleaned_data.get("email")
        # mengecek apakah ada string "item" di nama Item
        if not email.endswith("@gmail.com"):
            raise forms.ValidationError("This is not a valid email")
        return email
            




# ref: https://docs.djangoproject.com/en/3.1/ref/forms/fields/
class RawItemForm(forms.Form):
    name        = forms.CharField(label='', widget=forms.Textarea(attrs={"placeholder": "your name"}))
    description = forms.CharField(required=False, widget=forms.Textarea(
                                                            attrs={
                                                                "placeholder": "your description",
                                                                "class": "new-class-name two",
                                                                "id": "my-id-for-textarea",
                                                                "rows": 15,
                                                                "cols": 100,
                                                            }
                                                        )
                                                    )
    price       = forms.DecimalField(initial=199.99) 