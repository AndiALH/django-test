from django.db import models
from django.urls import reverse

# Create your models here.
# ref : https://docs.djangoproject.com/en/3.1/topics/db/models/
class Item(models.Model):
    name = models.CharField(max_length=100) # max_length = required
    description = models.TextField(blank=True, null=True)
    price = models.DecimalField(decimal_places=2, max_digits=100000000)
    summary = models.TextField(blank=False, null=False)
    featured = models.BooleanField(default=False) #null=True, default=True

    def get_absolute_url(self):
        # return f"/item/{self.id}/"
        return reverse("homepage:itemdesc", kwargs={"id": self.id})