from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect
from django.http import Http404
from .models import Item
from .forms import ItemForm, RawItemForm

# Create your views here.
# def authenticate_user(request):
#     if request.user.is_authenticated():
        

def index(request):
    dummy = {
        "animal": "doggy",
        "weapon": "glock",
        "health": 1000,
    }
    # return render(request, 'index.html', {})
    return render(request, 'index.html', dummy)

def library(request):
    books = {
        "history": ["1945", "9/11", "Tiananmen Square"],
        "geography": ["earth before pearl harbor", "Asia"],
        "chinese_cartoon": ["Naburo", "SAsaske"],
        "codes": [123,537,69,210,432],
        "test_html": "<h3>This is just a normal header 3 text</h3>"
    }
    # return render(request, 'index.html', {})
    return render(request, 'library.html', books)

def itemlist(request):
    # ref : https://stackoverflow.com/questions/3090302/how-do-i-get-the-object-if-it-exists-or-none-if-it-does-not-exist
    query_set = Item.objects.all()
    data = {
        # "name": obj.name,
        # "description": obj.description,
        # "price": obj.price
        "item_list": query_set
        }
    return render(request, 'itemlist.html', data)

#forms for item with django modelforms (automatic)
def item_create_view(request):
    form = ItemForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = ItemForm()

    context = {
        'form': form
    }
    return render(request, 'create_item.html', context)

def item_desc(request, id):
    # obj = Item.objects.get(id=id)
    #if you want to avoid DoesNotExist
    #use try except and raise Http404 or
    # obj = Item.objects.filter(id=id).first() #ref:https://stackoverflow.com/a/3090342
    #or
    obj = get_object_or_404(Item, id=id)
    context = {
        "object" : obj
    }
    return render(request, "item.html", context)

#forms for item with django forms (manual/raw)
# def item_create_view(request):
#     my_form = RawItemForm()
#     if request.method == "POST":
#         my_form = RawItemForm(request.POST)
#         if my_form.is_valid():
#             #now the data is good
#             print(my_form.cleaned_data) #for testing
#             Item.objects.create(**my_form.cleaned_data)
#         else:
#             print(my_form.errors) #for testing
#     context = {
#         'form': my_form 
#     }
#     return render(request, 'create_item.html', context)

def item_delete(request, id):
    # obj = Item.objects.get(id=id)
    #if you want to avoid DoesNotExist
    #use try except and raise Http404 or
    # obj = Item.objects.filter(id=id).first() #ref:https://stackoverflow.com/a/3090342
    #or
    obj = get_object_or_404(Item, id=id)
    if request.method == "POST":
        #confirming delete
        obj.delete()
        return redirect('../../../itemlist/') 
    context = {
        "object" : obj
    }
    return render(request, "item_delete.html", context)

def testing_templates(request):
    return render(request, "test_template.html")