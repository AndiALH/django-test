from django.db import models
from django.urls import reverse
from datetime import datetime

# Create your models here.

# Create your models here.
# ref : https://docs.djangoproject.com/en/3.1/topics/db/models/
class Article(models.Model):
    title = models.CharField(max_length=100) # max_length = required
    author = models.CharField(max_length=100, default="anonymous") # max_length = required
    content = models.TextField(default="no content yet")
    date_time = models.DateTimeField(default=datetime.now())
    featured = models.BooleanField(default=False) #null=True, default=True

    def get_absolute_url(self):
        # return f"/blog/{self.id}/"
        return reverse("blog:article-detail", kwargs={"id": self.id})