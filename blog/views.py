from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect
from django.http import Http404
from .models import Article
from .forms import ArticleForm
from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView

# Create your views here.
class ArticleCreateView(CreateView):
    template_name = 'article_create.html'
    form_class = ArticleForm
    queryset = Article.objects.all() #<app>/<modelname>_list.html (ex. blog/article_list.html)

    def form_valid(self, form):
        # print(form.cleaned_data)
        return super().form_valid(form)

class ArticleListView(ListView):
    template_name = 'article_list.html'
    queryset = Article.objects.all() #<app>/<modelname>_list.html (ex. blog/article_list.html)

class ArticleUpdateView(UpdateView):
    template_name = 'article_create.html'
    form_class = ArticleForm

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Article, id=id_)

    def form_valid(self, form):
        # print(form.cleaned_data)
        return super().form_valid(form)

class ArticleDetailView(DetailView):
    template_name = 'article_detail.html'
    queryset = Article.objects.all()
    # queryset = Article.objects.filter(id__gt=1)

    #use this if you use argument other than the default (pk) in the url
    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Article, id=id_)

class ArticleDeleteView(DeleteView):
    template_name = 'article_delete.html'
    queryset = Article.objects.all()
    # queryset = Article.objects.filter(id__gt=1)

    #use this if you use argument other than the default (pk) in the url
    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Article, id=id_)