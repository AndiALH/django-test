from django import forms

from .models import Article

class ArticleForm(forms.ModelForm):
    # title = forms.CharField(widget=forms.Textarea(attrs={"placeholder": "title", "rows": 1}))
    # author = forms.CharField(widget=forms.Textarea(attrs={"placeholder": "anon", "rows": 1}))
    # content = forms.CharField(required=False, widget=forms.Textarea(
    #                                                         attrs={
    #                                                             "placeholder": "content here",
    #                                                             "class": "new-class-name two",
    #                                                             "id": "my-id-for-textarea",
    #                                                             "rows": 15,
    #                                                             "cols": 100,
    #                                                         }
    #                                                     )
    #                                                 )
    # # ref: https://www.geeksforgeeks.org/datetimefield-django-forms/
    # date_time = forms.DateTimeField()
    class Meta:
        model = Article
        fields = [
            'title',
            'author',
            'content',
            'date_time',
        ]